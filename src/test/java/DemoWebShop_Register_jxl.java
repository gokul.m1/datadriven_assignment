import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DemoWebShop_Register_jxl {
	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");

		File fs = new File("/home/ranjith/Documents/Assignment1/Gokul.xls");
		FileInputStream input = new FileInputStream(fs);
		Workbook book = Workbook.getWorkbook(input);
		Sheet sheet = book.getSheet("Sheet1");
		int rows = sheet.getRows();
		int columns = sheet.getColumns();
		// for (int i = 0; i<rows; i++) {
		String firstname = sheet.getCell(0, 0).getContents();
		String lastname = sheet.getCell(0, 1).getContents();
		String mai = sheet.getCell(0, 2).getContents();
		String passwords = sheet.getCell(0, 3).getContents();
		String passwords1 = sheet.getCell(0, 3).getContents();

		driver.findElement(By.partialLinkText("Register")).click();
		driver.findElement(By.id("FirstName")).sendKeys(firstname);
		driver.findElement(By.id("LastName")).sendKeys(lastname);
		driver.findElement(By.id("Email")).sendKeys(mai);
		driver.findElement(By.name("Password")).sendKeys(passwords);
		driver.findElement(By.name("ConfirmPassword")).sendKeys(passwords1);
		driver.findElement(By.id("register-button")).click();

	}

}
